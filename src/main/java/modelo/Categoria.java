package modelo;

public class Categoria {
    private String nombre;
    private int categoria_id;
    private String tipo;

    public Categoria(int categoria_id,String nombre,String tipo) {
        this.nombre = nombre;
        this.categoria_id= categoria_id;
        this.tipo= tipo;
    }
    public Categoria(){

    }

    public int getCategoria_id() {
        return categoria_id;
    }

    public void setCategoria_id(int categoria_id) {
        this.categoria_id = categoria_id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getNombre() {
        return nombre;
    }

    public int getId() {
        return categoria_id;
    }

    public void setId(int id) {
        this.categoria_id = id;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "Categoria{" +
                "nombre='" + nombre + '\'' +
                ", categoria_id=" + categoria_id +
                ", tipo='" + tipo + '\'' +
                '}';
    }
}
