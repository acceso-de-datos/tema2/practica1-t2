package modelo;

public class Usuario {
    private int id;
    private String email;
    private String contraseña;
    private  String nombre;
    private String apellidos;
    private String dni;
    private  String domicilio;
    private String poblacion;
    private String provincia;
    private String telefono;
    private String tarjeta;


    public Usuario(int id, String email, String contraseña, String nombre, String apellidos, String dni, String domicilio, String poblacion, String provincia, String telefono, String tarjeta) {
        this.id = id;
        this.email = email;
        this.contraseña = contraseña;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.dni = dni;
        this.domicilio = domicilio;
        this.poblacion = poblacion;
        this.provincia = provincia;
        this.telefono = telefono;
        this.tarjeta = tarjeta;
    }

    public Usuario(String email, String contraseña, String nombre, String apellidos, String dni, String domicilio, String poblacion, String provincia, String telefono, String tarjeta) {

        this.email = email;
        this.contraseña = contraseña;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.dni = dni;
        this.domicilio = domicilio;
        this.poblacion = poblacion;
        this.provincia = provincia;
        this.telefono = telefono;
        this.tarjeta = tarjeta;
    }


    public int getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getContraseña() {
        return contraseña;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public String getDni() {
        return dni;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public String getPoblacion() {
        return poblacion;
    }

    public String getProvincia() {
        return provincia;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getTarjeta() {
        return tarjeta;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public void setPoblacion(String poblacion) {
        this.poblacion = poblacion;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public void setTarjeta(String tarjeta) {
        this.tarjeta = tarjeta;
    }

    @Override
    public String toString() {
        return "usuario{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", contraseña=" + contraseña +
                ", nombre='" + nombre + '\'' +
                ", apellidos='" + apellidos + '\'' +
                ", dni=" + dni +
                ", domicilio='" + domicilio + '\'' +
                ", poblacion='" + poblacion + '\'' +
                ", provincia='" + provincia + '\'' +
                ", telefono=" + telefono +
                ", tarjeta='" + tarjeta + '\'' +
                '}';
    }
}
