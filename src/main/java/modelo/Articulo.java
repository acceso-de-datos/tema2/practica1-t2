package modelo;

public class Articulo {

	private String nombre;
	private float precio;

	private int id;
	private int stock;
	private  String fabricante;
	private String tipoCol1;
	private String tipoCol2;

	public Articulo() {

	}

	public Articulo(String nombre, String fabricante ,float precio, int stock) {

		this.nombre = nombre;
		this.fabricante= fabricante;
		this.precio = precio;
		this.stock= stock;

	}

	public Articulo(int id, String nombre, String fabricante ,float precio, int stock,String tipoCol1,String tipoCol2) {
		this.id = id;

		this.nombre = nombre;
		this.fabricante= fabricante;
		this.precio = precio;
		this.stock= stock;
		this.tipoCol1= tipoCol1;
		this.tipoCol2= tipoCol2;

	}
	public Articulo(int id, String nombre, String fabricante ,float precio, int stock) {
		this.id = id;

		this.nombre = nombre;
		this.fabricante= fabricante;
		this.precio = precio;
		this.stock= stock;

	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public float getPrecio() {
		return precio;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public String getFabricante() {
		return fabricante;
	}

	public void setFabricante(String fabricante) {
		this.fabricante = fabricante;
	}

	public void setPrecio(float precio) {
		this.precio = precio;
	}

	public String getTipoCol1() {
		return tipoCol1;
	}

	public void setTipoCol1(String tipoCol1) {
		this.tipoCol1 = tipoCol1;
	}

	public String getTipoCol2() {
		return tipoCol2;
	}

	public void setTipoCol2(String tipoCol2) {
		this.tipoCol2 = tipoCol2;
	}

	@Override
	public String toString() {
		return "Articulo{" +
				"nombre='" + nombre + '\'' +
				", precio=" + precio +
				", id=" + id +
				", stock=" + stock +
				", fabricante='" + fabricante + '\'' +
				", tipoCol1='" + tipoCol1 + '\'' +
				", tipoCol2='" + tipoCol2 + '\'' +
				'}';
	}
}
