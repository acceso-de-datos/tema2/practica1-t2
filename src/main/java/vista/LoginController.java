package vista;

import dao.UsuarioDAO;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import modelo.Usuario;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;



public class LoginController<User> implements Initializable {

    private Usuario usuario;
    @FXML
    private TextField tfuser;

    @FXML
    private TextField tfpass;

    private UsuarioDAO usr;

    @FXML
    void login(ActionEvent event) throws IOException {

        String hash;
        hash= getsha1(tfpass.getText().toString());
        try {
            usr= new UsuarioDAO();

          usuario=usr.find(tfuser.getText(),hash);
        } catch (SQLException ex) {
            System.out.checkError();
            Platform.exit();
        }



        if(usuario.getContraseña().equals(hash)){
            Stage stageProd = new Stage();
            FXMLLoader loader = new FXMLLoader();
            AnchorPane root = (AnchorPane)loader.load(getClass().getResource("productos.fxml").openStream());
            ProductosController prodController = (ProductosController)loader.getController();
            prodController.setUsuario(usuario);
            Scene scene = new Scene(root);
            stageProd.setScene(scene);
            stageProd.alwaysOnTopProperty();
            stageProd.initModality(Modality.APPLICATION_MODAL);
            stageProd.show();
        }

    }

    @FXML
    void register(ActionEvent event) {

    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public static String getsha1(String input) {
        String sha1 = "";

        // With the java libraries
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-1");
            digest.reset();
            digest.update(input.getBytes("utf8"));
            sha1 = String.format("%040x", new BigInteger(1, digest.digest()));
        } catch (Exception e) {
            e.printStackTrace();

        }
        return sha1;
    }
    
}
