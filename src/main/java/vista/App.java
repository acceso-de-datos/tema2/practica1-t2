package vista;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * JavaFX App
 */
public class App extends Application {

    private static Scene scene;

    @Override
    public void start(Stage stage) throws IOException {
        scene = new Scene(loadFXML("login"), 640, 480);
        stage.setScene(scene);
        stage.show();
    }

    static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
        launch();
    }

}

/*
<?xml version="1.0" encoding="UTF-8"?>

<?import javafx.scene.control.Button?>
<?import javafx.scene.control.TextField?>
<?import javafx.scene.image.Image?>
<?import javafx.scene.image.ImageView?>
<?import javafx.scene.layout.AnchorPane?>
<?import javafx.scene.layout.Pane?>
<?import javafx.scene.layout.TilePane?>
<?import javafx.scene.text.Font?>
<?import javafx.scene.text.Text?>


<AnchorPane prefHeight="400.0" prefWidth="600.0" xmlns="http://javafx.com/javafx/11.0.1" xmlns:fx="http://javafx.com/fxml/1" fx:controller="org.openjfx.LoginController">
   <children>
      <TilePane layoutX="-2.0" layoutY="-5.0" prefHeight="400.0" prefWidth="593.0">
         <children>
            <ImageView fitHeight="175.0" fitWidth="175.0" pickOnBounds="true" preserveRatio="true">
               <image>
                  <Image url="../images/logo.jpg" />
               </image>
            </ImageView>
            <Text strokeType="OUTSIDE" strokeWidth="0.0" text="TU TIENDA DE INFORMATICA-PC CPMPONENTES" textAlignment="CENTER" wrappingWidth="622.47705078125">
               <font>
                  <Font size="16.0" />
               </font>
            </Text>
            <Pane prefHeight="226.0" prefWidth="600.0">
               <children>
                  <Text layoutX="72.0" layoutY="36.0" strokeType="OUTSIDE" strokeWidth="0.0" text="Usuario" />
                  <Text layoutX="82.0" layoutY="117.0" strokeType="OUTSIDE" strokeWidth="0.0" text="Contraseña" />
                  <TextField layoutX="256.0" layoutY="26.0" />
                  <TextField layoutX="249.0" layoutY="82.0" />
                  <Button layoutX="27.0" layoutY="130.0" mnemonicParsing="false" prefHeight="38.0" prefWidth="550.0" text="Entrar">
                     <font>
                        <Font size="21.0" />
                     </font>
                  </Button>
                  <Button layoutX="27.0" layoutY="174.0" mnemonicParsing="false" prefHeight="38.0" prefWidth="550.0" text="Registrar">
                     <font>
                        <Font size="21.0" />
                     </font>
                  </Button>
               </children>
            </Pane>
         </children>
      </TilePane>
   </children>
</AnchorPane>
*/