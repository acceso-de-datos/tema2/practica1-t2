package vista;

import dao.ArticuloDAO;
import dao.CategoriaDAO;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableListBase;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Slider;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import modelo.Articulo;

import modelo.Categoria;
import modelo.Grupo;
import modelo.Usuario;

import java.net.URL;
import java.sql.SQLException;
import java.sql.SQLOutput;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class ProductosController implements Initializable {
    private Usuario usuario;
    @FXML
    private TableView<Categoria> tvCategory;
    @FXML
    private TableView<Articulo> tvArt;

    @FXML
    private TableColumn<Categoria, String> Categorias;

    @FXML
    private TableColumn<Articulo, String> grNomb;

    @FXML
    private TableColumn<Articulo, String> grFabric;

    @FXML
    private TableColumn<Articulo, String> grPrice;

    @FXML
    private TableColumn<Articulo, String> grStock;
    @FXML
    private TableColumn<String, String> carrito;
    @FXML
    private TableView<String> tvCarrito;

    @FXML
    private Slider hasta;
    @FXML
    private Slider desde;
    @FXML
    private Text col2;
    @FXML
    private Text col1;

    @FXML
    private ChoiceBox<String> subcategorias2;
    @FXML
    private ChoiceBox<String> subcategorias1;
    @FXML
    private ChoiceBox<String> fabricante;

    private List<Articulo> ListArticulo;
    private List<Categoria> ListCategoria;
    private List<Articulo> ListArticuloFiltrados;
    private List<Articulo> ListArticuloFiltrados2;
    private List<String> ListFabricantes;

    private List<String> NameColumn;
    private List<String> ValueColumn1;
    private List<String> ValueColumn2;

    private ArticuloDAO artDAO;
    private Articulo articulo;
    private Categoria categoria;
    private CategoriaDAO catDAO;
    private float preciomin = 0;
    private float preciomax=0;

    public void setUsuario(Usuario user) {
        usuario = user;

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        Categorias.setCellValueFactory(new PropertyValueFactory<Categoria, String>("nombre"));
        grNomb.setCellValueFactory(new PropertyValueFactory<Articulo, String>("Nombre"));
        grPrice.setCellValueFactory(new PropertyValueFactory<Articulo, String>("Precio"));
        grStock.setCellValueFactory(new PropertyValueFactory<Articulo, String>("Stock"));
        grFabric.setCellValueFactory(new PropertyValueFactory<Articulo, String>("Fabricante"));
        carrito.setCellValueFactory(new PropertyValueFactory<String, String>("carrito"));
        ListArticuloFiltrados = new ArrayList<>();
        ListArticuloFiltrados2 = new ArrayList<>();
        ValueColumn1 = new ArrayList<>();
        ValueColumn2 = new ArrayList<>();
        ListFabricantes = new ArrayList<>();

        try {
            artDAO = new ArticuloDAO();

            catDAO = new CategoriaDAO();
            ListCategoria = catDAO.findAll();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
            Platform.exit();
        }

        //PARA INSERTAR aRTICULOS VINCULANDO LAS COLUMNAS




        //METEMOS LAS CATEGORIAS
        ObservableList<modelo.Categoria> listCategory = FXCollections.observableArrayList(ListCategoria);



        tvCategory.setItems(listCategory);
        tvCategory.setOnMouseClicked(new EventHandler<Event>() {


            public void handle(Event event) {
                //cogemos el seleccionado
                try {
                    ListArticulo = artDAO.findOfCategory(tvCategory.getSelectionModel().getSelectedItems().get(0).getId());
                    ObservableList<Articulo> lists = FXCollections.observableArrayList(ListArticulo);
                    tvArt.setItems(lists);



            //SOLO SE PUEDE PONER EL FILTRADO PRECIO CUANDO HAY PRODUCTOS SI NO N TIENE SENTIDO FILTRAR SIN PRODUCTOS
                    desde.valueProperty().addListener(new ChangeListener<Number>() {
                        public void changed(ObservableValue<? extends Number> ov,
                                            Number old_val, Number new_val) {
                            hasta.setMin(new_val.floatValue());
                            preciomin  = new_val.floatValue();

                        }
                    });

                    hasta.valueProperty().addListener(new ChangeListener<Number>() {
                        public void changed(ObservableValue<? extends Number> ov,
                                            Number old_val, Number new_val) {
                            System.out.println("entraa");
                            preciomax  = new_val.floatValue();
                            System.out.println(new_val.floatValue());

                            //LLAMAMOS PARA FILTRAR POR PRECIO CUANDO YA TENEMOS LOS DOS VALORES ELEJIDOS
                            pintarFilter("Fabricante",2,preciomax,preciomin);

                        }
                    });

                    for (Categoria cat: ListCategoria) {

                        //metemos las subcategorias
                        if(cat.getNombre().equals(tvCategory.getSelectionModel().getSelectedItems().get(0).getNombre())){
                            NameColumn=artDAO.findColumn(cat.getTipo());
                            if (cat.getTipo()!=null ) {

                                col1.setText(NameColumn.get(0));
                                col2.setText(NameColumn.get(1));
                                ValueColumn1 = artDAO.findColumnValue(cat.getTipo(),NameColumn.get(0));
                                ValueColumn2 = artDAO.findColumnValue(cat.getTipo(),NameColumn.get(1));

                                try {
                                    //guardamos los articulos con sus dos valores de filtrado de columnas
                                    ListArticuloFiltrados=artDAO.findOfValueCategory(cat.getTipo(),NameColumn.get(0),NameColumn.get(1),ListArticulo);

                                } catch (SQLException throwables) {
                                    throwables.printStackTrace();
                                }
                            }
                            //CAMBIO AQUI
                            for (Articulo art: ListArticulo
                                 ) {
                                ListFabricantes.add(art.getFabricante());
                            }
                            //Para quitar los repetidos y tener 1 fabricante de cada
                            Set<String> hashSet = new HashSet<String>(ListFabricantes);
                            ListFabricantes.clear();
                            ListFabricantes.addAll(hashSet);
                            ObservableList<String> nameFAbric = FXCollections.observableArrayList(ListFabricantes);

                            //Metemos los nombres de los fabricantes para el filtrado
                            fabricante.setItems(nameFAbric);

                            //Escuchadores para poder saber cual seleccionamos y filtrar
                            fabricante.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
                                @Override
                                public void changed(ObservableValue<? extends Number> observableValue, Number number, Number number2) {
                                    if(!((Integer)number2 ==(-1))) {
                                        pintarFilter(fabricante.getItems().get((Integer) number2),3,0,0);}

                                }
                            });
                            ObservableList<String> nameSubFilter1 = FXCollections.observableArrayList(ValueColumn1);
                            subcategorias1.setItems(nameSubFilter1);

                            ObservableList<String> nameSubFilter2 = FXCollections.observableArrayList(ValueColumn2);
                            subcategorias2.setItems(nameSubFilter2);

                            //Escuchadores para poder saber cual seleccionamos y filtrar
                            subcategorias1.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
                                @Override
                                public void changed(ObservableValue<? extends Number> observableValue, Number number, Number number2) {
                                    if(!((Integer)number2 ==(-1))) {
                                    pintarFilter(subcategorias1.getItems().get((Integer) number2),0,0,0);}

                                }
                            });


                            //Escuchadores para poder saber cual seleccionamos y filtrar
                            subcategorias2.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
                                @Override
                                public void changed(ObservableValue<? extends Number> observableValue, Number number, Number number2) {
                if(!((Integer)number2 ==(-1))) {
                    pintarFilter(subcategorias2.getItems().get((Integer) number2), 1,0,0);
                }
                                }
                            });



                        }
                    }


                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }

            }

        });

    }
    public void pintarFilter(String filter,int posicion,float preciomax,float preciomin){

List<Articulo>orderProd = new ArrayList<>();
switch (posicion){
    case  0:

        for (Articulo art: ListArticuloFiltrados
        ) {
            if(art.getTipoCol1().equals(filter)){
                orderProd.add(art);
            }
        }
        break;
    case 1:
        for (Articulo art: ListArticuloFiltrados
        ) {
            if (art.getTipoCol2().equals(filter)) {
                orderProd.add(art);
            }
        }
        break;
    case 2 :
        for (Articulo art: ListArticuloFiltrados
        ) {
            if (art.getPrecio()<=preciomax && art.getPrecio()>=preciomin) {

                orderProd.add(art);
            }
        }
        break;
    default:
        for (Articulo art: ListArticuloFiltrados
        ) {
            if(art.getFabricante().equals(filter)){
                orderProd.add(art);
            }
        }
        break;


}



        ObservableList<Articulo> lists = FXCollections.observableArrayList(orderProd);
        tvArt.setItems(lists);

    }
}
