package dao;

import modelo.Categoria;
import modelo.Usuario;

import java.sql.*;
import java.util.List;

public class UsuarioDAO implements GenericDAO<Usuario> {


    final String SQLSELECTALL = "SELECT * FROM articulos";
    final String SQLSELECTPK = "SELECT * FROM articulos WHERE id = ?";
    final String SQLINSERT = "INSERT INTO articulos (nombre, precio, codigo, grupo) VALUES (?, ?, ?, ?)";

    final String SQLFINDBYEXAMPLE = "SELECT * FROM usuario WHERE email like ? AND  contrasenya like ?";


    private final PreparedStatement pstSelectPK;
    private final PreparedStatement pstSelectAll;
    private PreparedStatement pstSelectByExample;
    private final PreparedStatement pstInsert;



    public UsuarioDAO() throws SQLException {
        Connection con = controladorbd.ConexionBD.getConexion();
        pstSelectPK = con.prepareStatement(SQLSELECTPK);
        pstSelectAll = con.prepareStatement(SQLSELECTALL);
        pstSelectByExample = con.prepareStatement(SQLFINDBYEXAMPLE);
        pstInsert = con.prepareStatement(SQLINSERT);


    }

    public void cerrar() throws SQLException {
        pstSelectPK.close();
        pstSelectAll.close();
        pstSelectByExample.close();
        pstInsert.close();

    }

    //AQUII


    public Usuario find(String email, String pass) throws SQLException {

        Usuario u = null;
        pstSelectByExample.setString(1, email);
        pstSelectByExample.setString(2, pass);
        ResultSet rs = pstSelectByExample.executeQuery();

        if (rs.first()) {
            return new Usuario(rs.getInt("usuario_id"), rs.getString("email"), rs.getString("contrasenya"), rs.getString("nombre"), rs.getString("apellidos"), rs.getString("dnicif"), rs.getString("domicilio"), rs.getString("poblacion"), rs.getString("provincia"), rs.getString("telefono"), rs.getString("tarjeta"));
        }
        rs.close();
        return u;
    }


    public String findCarrito(String email, String pass) throws SQLException {
        String carrito = "";
        return carrito;

    }


    //HASTA AQUIIII


    @Override
    public Usuario findByPK(int id) throws Exception {
        return null;
    }

    @Override
    public List<Usuario> findAll() throws Exception {
        return null;
    }

    @Override
    public List<Usuario> findByExample(Usuario t) throws Exception {
        return null;
    }


}
