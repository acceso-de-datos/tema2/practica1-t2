package dao;

import modelo.Grupo;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class GrupoDAO implements GenericDAO<Grupo> {
	final String SQLSELECTALL = "SELECT * FROM grupos";
	final String SQLSELECTPK = "SELECT * FROM grupos WHERE id = ?";
	final String SQLINSERT = "INSERT INTO grupos (descripcion) VALUES (?)";
	final String SQLUPDATE = "UPDATE grupos SET descripcion = ? WHERE id = ?";
	final String SQLDELETE = "DELETE FROM grupos WHERE id = ?";
	private final PreparedStatement pstSelectPK;
	private final PreparedStatement pstSelectAll;
	private PreparedStatement pstSelect ;
	private final PreparedStatement pstInsert;
	private final PreparedStatement pstInsertGenKey;
	private final PreparedStatement pstUpdate;
	private final PreparedStatement pstDelete;
	
	public GrupoDAO() throws SQLException {
		Connection con = controladorbd.ConexionBD.getConexion();
		pstSelectPK = con.prepareStatement(SQLSELECTPK);
		pstSelectAll = con.prepareStatement(SQLSELECTALL);
		pstInsert = con.prepareStatement(SQLINSERT);
		pstInsertGenKey =con.prepareStatement(SQLINSERT, Statement.RETURN_GENERATED_KEYS);
		pstUpdate = con.prepareStatement(SQLUPDATE);
		pstDelete = con.prepareStatement(SQLDELETE);
	}
	public void cerrar() throws SQLException {
		pstSelectPK.close();
		pstSelectAll.close();
		pstInsert.close();
		pstUpdate.close();
		pstDelete.close();
	}

	@Override
	public Grupo findByPK(int id) throws SQLException {
		Grupo gr = null;

		pstSelectPK.setInt(1, id);
		ResultSet rs = pstSelectPK.executeQuery();
		if (rs.first()) {
			return new Grupo(id,rs.getString("descripcion"));
		}
		rs.close();
		return gr;
	
	}

	@Override
	public List<Grupo> findAll() throws SQLException {
		List<Grupo> listaGrupo = new ArrayList<Grupo>();
		ResultSet rs = pstSelectAll.executeQuery();
		while (rs.next()) {
			listaGrupo.add(new Grupo(rs.getInt("id"),rs.getString("descripcion")));
		}
		rs.close();
		return listaGrupo;
	}

	@Override
	public List<Grupo> findByExample(Grupo grup) throws SQLException {
		String sql = "";
		Connection con = controladorbd.ConexionBD.getConexion();
		List<Grupo>listaGrupos = new ArrayList<Grupo>();
		if(grup.getDescripcion()!=null){
			sql="SELECT * FROM grupos WHERE descripcion like '%"+grup.getDescripcion()+"%'";
		}

		pstSelect=con.prepareStatement(sql);
		ResultSet rs = pstSelect.executeQuery();
		if (rs.first()) {
			listaGrupos.add(new Grupo(rs.getInt("id"), rs.getString("descripcion")));
		}

		return listaGrupos;
	}





}
