package dao;

import modelo.Articulo;
import modelo.Categoria;
import modelo.Grupo;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ArticuloDAO implements GenericDAO<Articulo> {

    final String SQLSELECTALL = "SELECT * FROM articulo";



    String SQLSELECCAT = "SELECT * FROM articulo a " +
            "INNER JOIN prodcategoria p ON a.articulo_id = p.articulo_id " +
            "WHERE p.categoria_id = ?";


    private PreparedStatement pstSelectAllCategory;




    private PreparedStatement pstSelectPK;
    private final PreparedStatement pstSelectAll;
    private PreparedStatement pstSelectByExample;
    private PreparedStatement pstGetValue;


    public ArticuloDAO() throws SQLException {
        Connection con = controladorbd.ConexionBD.getConexion();

        pstSelectAllCategory = con.prepareStatement(SQLSELECCAT);

        pstSelectAll = con.prepareStatement(SQLSELECTALL);


    }

    public void cerrar() throws SQLException {
        pstSelectPK.close();
        pstSelectAll.close();
        pstSelectByExample.close();
        pstGetValue.close();

    }

    @Override
    public Articulo findByPK(int id) throws Exception {
        return null;
    }

    @Override
    public List<Articulo> findAll() throws SQLException {

        List<Articulo> listaArticulos = new ArrayList<Articulo>();
        ResultSet rs = pstSelectAll.executeQuery();
        while (rs.next()) {
            listaArticulos.add(new Articulo(rs.getInt("articulo_id"), rs.getString("nombre"), rs.getString("fabricante"), rs.getFloat("precio"), rs.getInt("stock")));
        }
        rs.close();
        return listaArticulos;
    }

    public List<Articulo> findOfCategory(int cat_id) throws SQLException {

        List<Articulo> listaArticulos = new ArrayList<Articulo>();
        pstSelectAllCategory.setInt(1, cat_id);
        ResultSet rs = pstSelectAllCategory.executeQuery();
        while (rs.next()) {
            listaArticulos.add(new Articulo(rs.getInt("articulo_id"), rs.getString("nombre"), rs.getString("fabricante"), rs.getFloat("precio"), rs.getInt("stock")));
        }
        rs.close();
        return listaArticulos;
    }

    public List<Articulo> findOfValueCategory(String nameTable, String col1, String col2, List<Articulo> listArt) throws SQLException {
        Connection con = controladorbd.ConexionBD.getConexion();
        String sql = "SELECT  * FROM " + nameTable;
        List<Articulo> listaArticulos = new ArrayList<Articulo>();
        pstSelectByExample = con.prepareStatement(sql);
        ResultSet rs = pstSelectByExample.executeQuery();

        while (rs.next()) {
            for (Articulo art : listArt) {
                if (rs.getInt("articulo_id") == art.getId()) {
                    listaArticulos.add(new Articulo(art.getId(), art.getNombre(), art.getFabricante(), art.getPrecio(), art.getStock(), rs.getString(col1), rs.getString(col2)));
                }
            }
        }
        rs.close();
        return listaArticulos;
    }

    public List<String> findColumn(String name) throws SQLException {
        int fuera = 0;
        List<String> nameFilter = new ArrayList<>();
        Connection con = controladorbd.ConexionBD.getConexion();
        String sql = "select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA = 'pccompov2' and TABLE_NAME = '" + name + "'  order by ORDINAL_POSITION";
        pstSelectPK = con.prepareStatement(sql);
        ResultSet rs = pstSelectPK.executeQuery();
        while (rs.next()) {

            if (!rs.getString("COLUMN_NAME").equals("articulo_id") && fuera <= 2) {

                nameFilter.add(rs.getString("COLUMN_NAME"));
            }
            fuera++;
        }
        return nameFilter;
    }

    public List<String> findColumnValue(String nameTable,String col) throws SQLException {
        List<String> nameFilter = new ArrayList<>();
        Connection con = controladorbd.ConexionBD.getConexion();
        String sql = "select * from " + nameTable + "";
        pstGetValue = con.prepareStatement(sql);
        ResultSet rs = pstGetValue.executeQuery();
        while (rs.next()) {
            nameFilter.add(rs.getString(col));

        }
        Set<String> hashSet = new HashSet<String>(nameFilter);
        nameFilter.clear();
        nameFilter.addAll(hashSet);
        return nameFilter;
    }


    @Override
    public List<Articulo> findByExample(Articulo t) throws Exception {
        return null;
    }





}
