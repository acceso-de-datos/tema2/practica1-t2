package dao;

import modelo.Categoria;
import modelo.Grupo;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CategoriaDAO implements GenericDAO<Categoria> {
	final String SQLSELECTALL = "SELECT * FROM categoria";
	final String SQLSELECTPK = "SELECT * FROM grupos WHERE id = ?";
	final String SQLINSERT = "INSERT INTO grupos (descripcion) VALUES (?)";
	final String SQLUPDATE = "UPDATE grupos SET descripcion = ? WHERE id = ?";
	final String SQLDELETE = "DELETE FROM grupos WHERE id = ?";
	private final PreparedStatement pstSelectPK;
	private final PreparedStatement pstSelectAll;
	private PreparedStatement pstSelect ;
	private final PreparedStatement pstInsert;
	private final PreparedStatement pstInsertGenKey;
	private final PreparedStatement pstUpdate;
	private final PreparedStatement pstDelete;
	
	public CategoriaDAO() throws SQLException {
		Connection con = controladorbd.ConexionBD.getConexion();
		pstSelectPK = con.prepareStatement(SQLSELECTPK);
		pstSelectAll = con.prepareStatement(SQLSELECTALL);
		pstInsert = con.prepareStatement(SQLINSERT);
		pstInsertGenKey =con.prepareStatement(SQLINSERT, Statement.RETURN_GENERATED_KEYS);
		pstUpdate = con.prepareStatement(SQLUPDATE);
		pstDelete = con.prepareStatement(SQLDELETE);
	}
	public void cerrar() throws SQLException {
		pstSelectPK.close();
		pstSelectAll.close();
		pstInsert.close();
		pstUpdate.close();
		pstDelete.close();
	}


	@Override
	public Categoria findByPK(int id) throws Exception {
		return null;
	}

	@Override
	public List<Categoria> findAll() throws SQLException {
		List<Categoria> listaCategoria= new ArrayList<Categoria>();
		ResultSet rs = pstSelectAll.executeQuery();
		while (rs.next()) {
			listaCategoria.add(new Categoria(rs.getInt("categoria_id"),rs.getString("nombre"),rs.getString("tabla")));
		}
		rs.close();
		return listaCategoria;
	}

	@Override
	public List<Categoria> findByExample(Categoria t) throws Exception {
		return null;
	}



}
