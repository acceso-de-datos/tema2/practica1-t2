package dao;

import modelo.Categoria;

import java.util.List;

public interface GenericDAO<Tipo> {
	Tipo findByPK(int id) throws Exception;
    
    List<Tipo> findAll() throws Exception;
    List<Tipo> findByExample(Tipo t) throws Exception;


    
}
