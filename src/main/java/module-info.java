module vista {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;

    opens vista to javafx.fxml;
    opens modelo to javafx.base;
    exports vista;
}